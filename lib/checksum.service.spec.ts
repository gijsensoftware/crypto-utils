import { ChecksumService } from './checksum.service';
import { CryptoService } from './crypto-service';

describe('ChecksumService', () => {
    const sharedChecksumSecret = 'some-shared-secret';
    let service: ChecksumService;

    beforeEach(() => {
        service = new ChecksumService(new CryptoService());
    });

    it('should calculate checksums', () => {
        expect(service.calculateChecksumHash('Testbericht', sharedChecksumSecret))
            .toStrictEqual('3fc4d1b034fc6c57a83471861dff8180f4e8d7e550ec2f6b5136eb208a1e6cb3');
    });

    it('should hash input sets', () => {
        const hashes = service.calculateChecksumHashes([
            {
                field: 'a-field',
                value: 'a-value',
            },
        ], sharedChecksumSecret);
        expect(hashes[0].field).toStrictEqual('a-field');
        expect(hashes[0].hash).toStrictEqual('8533e3fd43745809d54470b86af2b60f115651d236865898c4726d0c1995cd52');
    });

    it('should verify checksums of input sets', () => {
        service.verify([
            {
                field: 'a-field',
                hash: '8533e3fd43745809d54470b86af2b60f115651d236865898c4726d0c1995cd52',
            },
        ], [
            {
                field: 'a-field',
                value: 'a-value',
            },
        ], sharedChecksumSecret);

        expect(() => {
            service.verify([
                {
                    field: 'a-field',
                    hash: '8533e3fd43745809d54470b86af2b60f115651d236865898c4726d0c1995cd52a',
                },
            ], [
                {
                    field: 'a-field',
                    value: 'a-value',
                },
            ], sharedChecksumSecret);
        }).toThrow();
    });
});
