import { testGuard } from '@gijsensoftware/typescript-guards';
import { isChecksum } from './Checksum';

describe('isChecksum', () => {
    testGuard(isChecksum, [
        {
            field: 'field-123',
            hash: 'hash-123',
        },
    ]);
});
