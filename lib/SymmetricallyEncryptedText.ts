import { isNonNullNonArrayObject, isString } from '@gijsensoftware/typescript-guards';

export interface SymmetricallyEncryptedText {
    cipher: string;
    key: string;
    iv: string;
}

export const isSymmetricallyEncryptedText = (input: unknown): input is SymmetricallyEncryptedText =>
    isNonNullNonArrayObject(input)
    && isString(input.cipher)
    && isString(input.key)
    && isString(input.iv);
