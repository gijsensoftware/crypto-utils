import { CryptoService } from './crypto-service';

const dummyEncryptedMessageSymmetric = {
    'cipher': 'r4KvOR7Z0d0RwChc38tTxg==',
    'key': 'WtpoB3R6rHSlJUkWgzhGw89QJSdvO3l7hr3/If16rm6w9eS2r01RqGjh0TLp8TnpUCdE3PvmuD0SCT7nyFRkCAmp6lH4H2FzZNv8xSINVLnKH8ttDBfkdUeLWVpiRQRRAHBBUvNAHIAuLognqsf9snJv2V7YhZQlCat3NNH0/4M=',
    'iv': 'Sg1xSP0lOXqhHOwpFwIXNuOtBBuQCDceO8ACqjJkdCbm84Urpsy93IfSW8HaKkc7R3FPqovpQ6/rJ74+Ul3sEciDaY1y3F74YnmcZls/YTs0d+NqaqSM98LaI5Iumi4APyGMFdRXZH+cYDzfmLKNFN8KAJemVrWcncSKg6U6cWI=',
}

const dummyEncryptedMessageAsymmetric = 'kL6GpTtr81hOiSE1FTAHDrhM1oOu5+nf/tHDkfRfTyoPkwqcwK2tw62B2e95vH7eOtl5WzHuOtZIJq7KVEWuKCXOxwFqULSDUsZtCDQETR+wJW8d5pbDzUB/lmmi4ShC/1wWdOcXnwKrhxQ1nMqMgeOVsXMAOEGGoDUu/AdtGb8=';

const dummyPrivateKey = '-----BEGIN RSA PRIVATE KEY-----\nMIICWwIBAAKBgQC3q7Qf/41iKP+zqEqqPJpDoQtF8TV/O5phqxgSWNLATsb3/Mlp\nUkTNbyCCVOeghVoh8b/avKmV0r5rAHEVB8G2TwnbOcF9R0FH5W6RKofiMfZq2cS4\nHnCAk78eVsFnOHkTMQ3FtPcotU+KPd5TV8WDkYwOkeSiNeLXw7Z+HcBALQIDAQAB\nAoGAZe/pD51QxbsFd5uenO73mzNFQ3NFArF81FoWCRC9HzhZm4bz7f1X7ECR5WPk\nDiYUySQYG/l7kYieYFqlaEueyXROlUyqEg4t7x8M+ilm+LAgztQ948BB42ZJmoz7\nDRUiixGkoHNTunaUnId3qecYW4F2FGfnk5b2gWsSzEszS80CQQDadRK4uwh8qjVW\nqWoojtu9A6fAgQOYUSWmgSNoriQbGY3XalEsOnU3MTTaH+oLYdNqCQlXVGgKn/Yg\nzB/4nV3jAkEA1zw13JNK1cC1Uqub+s6TyG6JNZEnR9DhvfIKDtc/QaonBBk3rqom\nY45L2GSzMautcFpZASHENIXY2UF9q0tGrwJAJiB0qgUKkCdss7lRq85zD2QJ7REX\nPYg8F/lF4skxyAEHvmbxz9RMDNvrq34SbPYf3cvB85nppqEQZdz6X3unnQJAG3tk\npTVpaMGiZVvkHrIhzIXQnzZZfaH+MPKmth5lxUykjrFpmrO/ExcoOAkjmEQDcUUX\n+D0HgG1WsubZ1PVXoQJAepL4FVY70ucHg4S1Lbtztx6ibT7vNBhbDw9XgbTRXT5U\nIYx89r5Sq5ALyNVs3vbnxhHgtTt/h5QtFUsCgY7DGA==\n-----END RSA PRIVATE KEY-----\n';

describe('Bn Crypto Service', () => {
    it('should allow instance creation', async () => {
        new CryptoService();
    });

    it('should decrypt asymmetrically encrypted messages', async () => {
        const service = new CryptoService();
        const decrypted = service.decrypt(dummyEncryptedMessageAsymmetric, dummyPrivateKey);
        expect(decrypted).toEqual('Testbericht');
    });

    it('should encrypt and decrypt messages', async () => {
        const service = new CryptoService();
        const cipherText = service.publicEncrypt('Testberichtje', dummyPrivateKey);
        const decrypted = service.decrypt(cipherText, dummyPrivateKey);
        expect(decrypted).toEqual('Testberichtje');
    });

    it('should return undefined on invalid ciphertext', async () => {
        const service = new CryptoService();
        const decrypted = service.decrypt('Foo', dummyPrivateKey);
        expect(decrypted).toBeUndefined();
    });

    it('should decrypt symmetrically encrypted messages', async () => {
        const service = new CryptoService();
        const decrypted = service.decryptSymmetric(dummyEncryptedMessageSymmetric, dummyPrivateKey);
        expect(decrypted).toEqual('Testbericht');
    });

    it('should hash values', () => {
        const service = new CryptoService();
        expect(service.hash('test')).toStrictEqual('9f86d081884c7d659a2feaa0c55ad015a3bf4f1b2b0b822cd15d6c15b0f00a08');
    });
});
