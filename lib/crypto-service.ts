import { SymmetricallyEncryptedText } from './SymmetricallyEncryptedText';

export class CryptoService {
    private crypto;

    constructor() {
        this.crypto = require('crypto');
    }

    public publicEncrypt(plainText: string,
                         publicKey: string): string {
        try {
            const key = {
                key: publicKey,
            };
            return this.crypto
                .publicEncrypt(key, Buffer.from(plainText, 'utf-8'))
                .toString('base64');
        } catch (e) {
            console.error('Error while encrypting: ' + e);
            throw e;
        }
    }

    public decrypt(cipher: string,
                   privateKey: string): string {
        try {
            const key = {
                key: privateKey,
            };
            return this.crypto
                .privateDecrypt(key, Buffer.from(cipher, 'base64'))
                .toString('utf8');
        } catch (e) {
            console.error('Error while decrypting: ' + e);
            return undefined;
        }
    }

    public decryptSymmetric(message: SymmetricallyEncryptedText,
                            privateKey: string): Promise<string> {
        const key = {
            key: privateKey,
        };

        const decryptedKey = this.crypto.privateDecrypt(key, Buffer.from(message.key, 'base64'));
        const decryptedIv = this.crypto.privateDecrypt(key, Buffer.from(message.iv, 'base64'));
        try {
            const cipherText = Buffer.from(message.cipher, 'base64');
            const decipherFunction = this.crypto.createDecipheriv('aes-256-cbc', decryptedKey, Buffer.from(decryptedIv));
            let decryptedData = decipherFunction.update(cipherText);
            decryptedData += decipherFunction.final('utf8');
            return decryptedData;
        } catch (e) {
            console.error('Error while decrypting: ' + e);
            return undefined;
        }
    }

    public hash(input: string,
                algorithm: string = 'sha256'): string {
        const hasher = this.crypto.createHash(algorithm);
        hasher.update(input);
        return hasher.digest('hex');
    }
}
