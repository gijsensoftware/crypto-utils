import { isNonNullNonArrayObject, isString } from '@gijsensoftware/typescript-guards';
import { Checksum } from './Checksum';
import { CryptoService } from './crypto-service';

export interface ChecksumInput {
    field: string;
    value: string;
}

export const isChecksumInput = (input: unknown): input is ChecksumInput =>
    isNonNullNonArrayObject(input)
    && isString(input.field)
    && isString(input.value);

export class ChecksumService {
    private checksumSecrets: Map<string, string>;

    constructor(private readonly cryptoService: CryptoService) {
        this.checksumSecrets = new Map<string, string>();
    }

    public calculateChecksumHash(input: string,
                                 checksumSecret: string): string {
        const saltedInput = input + checksumSecret;
        return this.cryptoService.hash(saltedInput);
    }

    public calculateChecksumHashes(inputs: ChecksumInput[],
                                   checksumSecret: string): Checksum[] {
        return inputs
            .map((input: ChecksumInput) => {
                const hash = this.calculateChecksumHash(input.value, checksumSecret);
                return {
                    field: input.field,
                    hash,
                }
            });
    }

    public verify(checksums: Checksum[],
                  values: ChecksumInput[],
                  checksumSecret: string): void {
        for (const input of values) {
            const checksum: Checksum | undefined = checksums
                .find(checksum => checksum.field === input.field);
            if (!checksum) {
                throw new Error('No checksum found for ' + input.field);
            }
            const hash = this.calculateChecksumHash(input.value, checksumSecret);
            if (hash !== checksum.hash) {
                throw new Error('Hash mismatch');
            }
        }
    }
}
