import { testGuard } from '@gijsensoftware/typescript-guards';
import { isSymmetricallyEncryptedText } from './SymmetricallyEncryptedText';

describe('isSymmetricallyEncryptedText', () => {
    testGuard(isSymmetricallyEncryptedText, [
        {
            cipher: 'cipher',
            key: 'key',
            iv: 'iv',
        },
    ]);
});
