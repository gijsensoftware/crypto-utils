import { isNonNullNonArrayObject, isString } from '@gijsensoftware/typescript-guards';

export interface Checksum {
    field: string;
    hash: string;
}

export const isChecksum = (input: unknown): input is Checksum =>
    isNonNullNonArrayObject(input)
    && isString(input.field)
    && isString(input.hash);
